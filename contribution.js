var contributions = (function () {

    var pageSize = 5;

    var contributions = [];

    var currentFilter = null;

    var init = function() {
        $('#load-spinner').hide();
        getContributions();
    }

    var updatePagination = function(currentPage, totalPages) { 
        console.log("Updating pagination. Page: " + currentPage + " Total pages: " + totalPages);       
        $('#pagination-section').twbsPagination('destroy'); 
        $('#pagination-section').twbsPagination({
            startPage: currentPage,
            totalPages: totalPages,
            initiateStartPageClick: false,
            onPageClick: onPageClick
        });
    }

    var onPageClick = function (event, page) { 
        console.log("Page selected: " + page);
        getContributions(page, currentFilter);
    }

    var getContributions = function(page, filter) {
        if (!page) page = 1;
        var startIndex = pageSize * (page - 1);
        $('#load-spinner').show();  
        scrollToContributions();      
        contributionService.getContributions(startIndex, pageSize, filter)
            .then(function(response) {
                console.log("Success!\n" + JSON.stringify(response));
                var totalPages = Math.ceil(response.totalCount / pageSize);
                updatePagination(page, totalPages);
                renderContributions(response.contributions);                
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log("Error!\n" + JSON.stringify(jqXHR));
            })
            .done(function() {
                $('#load-spinner').hide();
            });
    }

    var renderContributions = function(contributions) {
        var contributionItems = contributions.map(function(contribution, index) {
            return {
                id: contribution.id,
                date: contribution.date,
                amount: contribution.amount,
                collapseHref: '#collapse' + index,
                collapseId: 'collapse' + index,
                isCollapsedIn: (index == 0) ? 'in' : ''
            }
        });
        $("#contributions-placeholder").loadTemplate("#contributions-list-item-template", contributionItems);
    }

    var scrollToContributions = function() {
        var contributionsTop = $('#contributions-container').offset().top;
        if ($(window).scrollTop() > contributionsTop) {
            $('html, body').animate({
                scrollTop: contributionsTop
            }, 500);
        }
    }

    return {
        init: init
    };
})();