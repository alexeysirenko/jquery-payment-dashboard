var contributionService = (function() {

    var url = "//localhost:3004/contributions";

    var getContributions = function(start, limit, filter) {
        console.log('Loading ' + limit + ' contributions starting from ' + start + ' with filter ' + JSON.stringify(filter));
        var paginatedUrl =  url + '?_start=' + start + '&_limit=' + limit;
        if (filter) {
            if (filter.id) {
                paginatedUrl += '&id=' + filter.id; 
            }
        }
        return $.ajax({
            url: paginatedUrl,
            dataType: "json"
        }).then(function(data, textStatus, jqXHR) {
            return {
                contributions: data,
                totalCount: Number(jqXHR.getResponseHeader('X-Total-Count'))
            }
        })
    }

    return {
        getContributions: getContributions
    }
})();